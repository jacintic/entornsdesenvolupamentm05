package veterinary;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

class ex1_TestsJacint {

	@Test
	void testWithDiscountCalculateCost() {
		VisitWithoutDiscount v = new VisitWithoutDiscount("a",'b');
		double  testPrice = 10.50;
		v.setGrossPrice(testPrice);
		assertEquals(v.calculateCost(),testPrice + testPrice * v.IVA / 100);
	}
	// discount for summer
	@Test
	void testWithoutDiscountCalculateCostSummer() {
		VisitWithDiscount v = new VisitWithDiscount("17/08/1986",'b');
		double  testPrice = 10.50;
		v.setGrossPrice(testPrice);
		// predicted summer discount
		double predictedSummerDiscount = 15;
		double priceCalculation = testPrice - testPrice * predictedSummerDiscount  / 100;
		priceCalculation += priceCalculation * 21 / 100;
		assertEquals(v.calculateCost(),priceCalculation);
	}
	
	// discount for summer second month
		@Test
		void testWithoutDiscountCalculateCostSummer2() {
			VisitWithDiscount v = new VisitWithDiscount("17/07/1986",'b');
			double  testPrice = 10.50;
			v.setGrossPrice(testPrice);
			// predicted summer discount
			double predictedSummerDiscount = 15;
			double priceCalculation = testPrice - testPrice * predictedSummerDiscount  / 100;
			priceCalculation += priceCalculation * 21 / 100;
			assertEquals(v.calculateCost(),priceCalculation);
		}
		
	// discount out of summer days
	@Test
	void testWithoutDiscountCalculateCostNonSummer() {
		VisitWithDiscount v = new VisitWithDiscount("17/09/1986",'b');
		double  testPrice = 10.50;
		v.setGrossPrice(testPrice);
		// predicted summer discount
		double predictedDiscount = 10;
		double priceCalculation = testPrice - testPrice * predictedDiscount  / 100;
		priceCalculation += priceCalculation * 21 / 100;
		assertEquals(v.calculateCost(),priceCalculation);
	}
	
}
