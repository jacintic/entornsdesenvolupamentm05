package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import root.Major;

class MajorTest {

	@Test
	public void CollectionRight() {
		int[] myList = { 3, 7, 9, 8 };
		Major m = new Major();
		assertEquals(9, Major.mayor(myList));
		System.out.println("@Test: FullArrayList");
	}

	// ORDER

	@Test
	public void Order1() {
		int[] myList = { 9, 7, 8 };
		Major m = new Major();
		assertEquals(9, Major.mayor(myList));
		System.out.println("@Test: FullArrayList");
	}

	@Test
	public void Order2() {
		int[] myList = { 7, 9, 8 };
		Major m = new Major();
		assertEquals(9, Major.mayor(myList));
		System.out.println("@Test: FullArrayList");
	}

	@Test
	public void Order3() {
		int[] myList = { 7, 8, 9 };
		Major m = new Major();
		assertEquals(9, Major.mayor(myList));
		System.out.println("@Test: FullArrayList");
	}

	// Dupilcates
	@Test
	public void Duplicates() {
		int[] myList = { 9, 7, 9, 8 };
		Major m = new Major();
		assertEquals(9, Major.mayor(myList));
		System.out.println("@Test: FullArrayList");
	}

	// Unique value
	@Test
	public void UniqueVal() {
		int[] myList = { 7 };
		Major m = new Major();
		assertEquals(7, Major.mayor(myList));
		System.out.println("@Test: FullArrayList");
	}

	// Negative value
	@Test
	public void NegativeVal() {
		int[] myList = {-4, -6, -7};
		Major m = new Major();
		assertEquals(-4, Major.mayor(myList));
		System.out.println("@Test: FullArrayList");
	}

	// Empty list throw exception
	@Test
	public void EmptyCollection() {
		int[] myList = {};
		Major m = new Major();
		// exception call
		Exception exception = assertThrows(RuntimeException.class, () -> {
			Major.mayor(myList);
		});
		String expectedMessage = "Llista buida";
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
		System.out.println("@Test: EmptyArrayList");
	}

}
