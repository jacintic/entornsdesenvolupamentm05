# Diagramas UML  
Unified Modeling Languag  
Hay que seguir los estandares, por ejemplo, la fecha de la herencia tiene la punta blanca.  
  
## Tipos de diagrama:  
* Estructura
* Comportament  
  
### Fases de desenvolupament i els seus diagrames  
* inciial (fase de requeriments) => diagrames de casos d'us i diagrames d'activitats
* fase d'analisi => diagrama de classes (p.e. entitat i relacció)
* fase de disseny => diagrames de sequencia i diagrames de comunicació
  
### Actors:  
Els tipus de rols que poden interactuar amb la nostra aplicació. Cada actor, que casos d'us puede llevar a cavo?  
  
### Casos d'us: 
Una elipse amb lineas, son les coses que poden fer els actors.  
Una funcionalitat, una taska que puede hacer la aplicación o el usuario/rol con la aplicación.  
Els noms dels casos d'us s'han de començar amb un verb.  
Espotposaraldavantdelnomdelsactorslaparaulaclau<<system>>siaquestssónprocessos externs.  
![exemple codificació cas d'us](casos_dus.png)  
  
### Asociacions entre actor i casos d'us:  
* include => es obligatori o sempre es fa (cas d'us A implica obligatoriament cas d'us B)
* extends => no es obligatori, a vegades passa (reservar llibre <== extends == denegar reserva)
* herencia => entre actors, herencia (soci de la biblioteca <|=== soci investigador), la punta de la fletxa d'herencia te la punta blanca (como standard).
  
### Limits del sistema:  
* els actors a la esquerra, de mes generic (top) a mes especialitzat (bottom)
  
## Exemple diagrama:  
![Diagrama de casos d'us](diagrama.png)
