package debugging;

public class Factorial {

	public static double factorial (double numero) {

		if (numero==0)
			return 1;
		else
		{
			double resultat = numero * factorial(numero-1);
			return resultat;
		}
	}
	
	public static double factorial2 (double numero) {
		int result = 1;
		for (int i = 5; i > 0; i--) {
			result *= i;
		}
		return result;
	}
	

	 public static void main(String[] args) {

		System.out.println(factorial(5));
		System.out.println(factorial2(5));
		
		
		
		
	}
}
