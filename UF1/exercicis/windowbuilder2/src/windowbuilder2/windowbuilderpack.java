package windowbuilder2;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class windowbuilderpack {

	protected Shell shell;
	private Text display;
	private Button btn_2;
	private Button btn_5;
	private Button btn_3;
	private Button btn_4;
	private Button btn_6;
	private Button btn_7;
	private Button btn_8;
	private Button btn_9;
	private Button btn_0;
	private Button btn_eq;
	private Button btn_plus;
	private Button btn_min;
	private Button btn_mul;
	private Button btn_div;
	private Button btn_clear;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			windowbuilderpack window = new windowbuilderpack();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	/**
	 * method to calculate operations
	 */
	public String getOpValue(String prev, String post, String oper) {
		String result = "";
		switch(oper) {
			case "+":
			result = "" + (Integer.parseInt(prev) + Integer.parseInt(post));
		}
		return result;
	}
	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(488, 424);
		shell.setText("SWT Application");
		
		display = new Text(shell, SWT.BORDER);
		display.setBounds(22, 32, 446, 34);
		
		Button btn_1 = new Button(shell, SWT.NONE);
		btn_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				display.setText(display.getText() + "1");
			}
		});
		btn_1.setBounds(22, 88, 46, 34);
		btn_1.setText("1");
		
		btn_2 = new Button(shell, SWT.NONE);
		btn_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				display.setText(display.getText() + "2");
			}
		});
		btn_2.setText("2");
		btn_2.setBounds(122, 88, 46, 34);
		
		btn_3 = new Button(shell, SWT.NONE);
		btn_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				
				display.setText(display.getText() + "3");
			}
		});
		btn_3.setText("3");
		btn_3.setBounds(222, 88, 46, 34);
		
		btn_4 = new Button(shell, SWT.NONE);
		btn_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				display.setText(display.getText() + "4");
			}
		});
		btn_4.setText("4");
		btn_4.setBounds(322, 88, 46, 34);
		
		btn_5 = new Button(shell, SWT.NONE);
		btn_5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				display.setText(display.getText() + "5");
			}
		});
		btn_5.setText("5");
		btn_5.setBounds(422, 88, 46, 34);
		
		btn_6 = new Button(shell, SWT.NONE);
		btn_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				display.setText(display.getText() + "6");
			}
		});
		btn_6.setText("6");
		btn_6.setBounds(23, 161, 46, 34);
		
		btn_7 = new Button(shell, SWT.NONE);
		btn_7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				display.setText(display.getText() + "7");
			}
		});
		btn_7.setText("7");
		btn_7.setBounds(123, 161, 46, 34);
		
		btn_8 = new Button(shell, SWT.NONE);
		btn_8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				display.setText(display.getText() + "8");
			}
		});
		btn_8.setText("8");
		btn_8.setBounds(223, 161, 46, 34);
		
		btn_9 = new Button(shell, SWT.NONE);
		btn_9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				display.setText(display.getText() + "9");
			}
		});
		btn_9.setText("9");
		btn_9.setBounds(323, 161, 46, 34);
		
		btn_0 = new Button(shell, SWT.NONE);
		btn_0.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				display.setText(display.getText() + "0");
			}
		});
		btn_0.setText("0");
		btn_0.setBounds(423, 161, 46, 34);
		
		btn_eq = new Button(shell, SWT.NONE);
		btn_eq.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine engine = manager.getEngineByName("js");
				Object result = null;
				try {
					result = engine.eval(display.getText());
				} catch (ScriptException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				display.setText("" + result);
			}
		});
		btn_eq.setText("=");
		btn_eq.setBounds(320, 345, 146, 34);
		
		btn_plus = new Button(shell, SWT.NONE);
		btn_plus.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				
			
				display.setText(display.getText() + "+");
			}
		});
		btn_plus.setText("+");
		btn_plus.setBounds(322, 230, 46, 34);
		
		btn_min = new Button(shell, SWT.NONE);
		btn_min.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
			
				display.setText(display.getText() + "-");
			}
		});
		btn_min.setText("-");
		btn_min.setBounds(422, 230, 46, 34);
		
		btn_mul = new Button(shell, SWT.NONE);
		btn_mul.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
			
				display.setText(display.getText() + "*");
			}
		});
		btn_mul.setText("*");
		btn_mul.setBounds(320, 298, 46, 34);
		
		btn_div = new Button(shell, SWT.NONE);
		btn_div.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
			
				display.setText(display.getText() + "/");
			}
		});
		btn_div.setText("/");
		btn_div.setBounds(420, 298, 46, 34);
		
		btn_clear = new Button(shell, SWT.NONE);
		btn_clear.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
			
				display.setText("");
			}
		});
		btn_clear.setText("Clear");
		btn_clear.setBounds(22, 298, 91, 81);

	}
}
