/*
 * Boat.java
 * 		
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

package harbour;

/**
 * Simulates a boat.
 */
public abstract class Boat {

    /** Boat registration plate */
    /** Boat length */
    private double length;
    /** Boat fabrication year */

    // Constructor

    // TODO

    public Boat(String regPlate2, double length2, int year2) {
		// TODO Auto-generated constructor stub
	}

	/**
     * Calculates the amount according to the boat.
     * 
     * @return the amount
     */
    public abstract double amountAccordingToBoat();

    /**
     * Calculates the base amount for all boats.
     * 
     * @return the amount
     */
    public double baseAmount() {
        return this.length * 10;
    }

    // Getters & setters
    
    // TODO

    // equals & hashCode
    
    // TODO

}
