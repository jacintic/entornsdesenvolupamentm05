# Com funciona Java  
El progrmador escriu un programa, el programa es un .java. Per que el programa sigui interpretat per la maquina virtual de Java, primer l'ha de compilar. Un cop compilat (javac nomPrograma.java) s'obte un fitcher .class que pot ser executat per la maquina virtual de Java, la comanda es java nomPrograma(.class). Per poder desenvolupar/ programar, es necesita un entorn JDK. L'entorn JDK inclou eines especifiques per poder desenvolupar. En especific, una maquina virtual de java, un compilador, una eina per realitzar la documentació (Javadoc), y un empaquetador (que fa els .jar).  
La maquina virtual de Java, tradueix els .class que estan en format bitecode y els tradueix a codi binari, cada VM de Java esta adaptada per a cada sistema. 
  
## JDK  
Es un entorn de desenvolupament, que te lo necesari per executar un programa compilat de Java (JRE) mes les eines extres de progrmació.  
  
## JRE  
Son les eines basiques per poder executar Java ja compilat, en especific el comand javac i la maquina virtual.  
  
## Java VM  
Es la part que executa les .class inclossa tant a JDK com a JRE. Tradueix els bitecode a codi binari i es adaptada a cada sistema operatiu a on es instalada.  
  
# Cicle de vida d'un programa Java  
* Primer es crea el programa i es un .java.
    * mitjançand l'ordre javac (compilació) nomPrograma.java es construeix un fitxer .class en format bitecode
* El fitxer .class esta llest per ser executat per la maquina virtual, esta en format bytecode (l'ordre ese java nomProgramaCompilat)
* La JVM transforma el .class en binari 
* JRE el programa ja en binari s'executa i es el que veiem per pantalla o qualsevol medi al que sigui adaptat