# M05 Entorns de Desenvolupament  
  
## Git  
  
### git log  
* git log --oneline #versión abreviada
* Opción-p para ver los cambios hechos en los ficheros
* --oneline
* --graph
* --decorate
* --author
* -n(ie: -3)
  
### pull revase  
Reescrivir:
1. primeros cambios en remote
2. mis canvios  
  
### git fetch  
  
### branches  
#### branch testing  
`git checkout testing` => vas a branca testing  
`git checkout master`  => tornas a la master amb lo que hi ha a la master  
Els commits de testing es guardan a testing, no els veuras desde master.  
  
### merge rebase  
1. aplicar cambios remotos
2. aplicar mis cambios  
  
### cleanup of obsolete branches and commits  
Trobar el balanç entre nateja i utilitat.  
  
### git diff  
Ver la diferencia entre el ultimo commit y lo que voy a subir  

### git revert  
copiar commit pasado en un nuevo commit
  
### git reset  
volver a un commit pasado borrando la historia "futura", se puede acavar recuperando pero tendras que hacer magia.  
  
### git tags  
```
Una tag/etiqueta de GIT se usa para identificar de una manera más sencilla un commit  
●Se suele usar para hacer públicas versiones SW  
●Para mostrar las etiquetas disponibles: git tag  
●Para crear una etiqueta en el commit actual: git tag -a nomEtiqueta -m 'Missatge'  
●Per crear una etiqueta en un commit anterior: git tag -a nombreEtiqueta -m 'Mensage' #idCommit○para saber el ID del commit: git log  
●Referencias: [link tags](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)

```
