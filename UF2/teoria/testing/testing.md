# Testing  
  
## Un tipo  
Programar orientado a cumplir los tests. Recomendable, puede ahorrar errores al principio.  
  
El principi de Pareto o llei 80-20 (el 80% dels efectes són conseqüència del 20% de les causes) és aplicable a les proves de programari.  

* El 80% dels errors està en el 20% dels mòduls.
* Cal identificar aquests mòduls i provar-los molt
  
Cuando hay tiempo, todos los testings que hagais mas pronto es tiempo que te ahorraras despues a la hora de desarrollar el software. "Problemes que despres t'estalvies".  
  
  
## Plan de pruevas  
  
Hay empresas que no tienen nada de pruevas y otras tienen un departamento entero.  
  
## Definiendo los casos de pruevas  
  
Ejemplo, funcion de año bisiesto. Provar:  
* uno que sea multiple de 4
* de 5
* etc
  
Muchas pruevas aseguran la solidez del programa.  
  
* manera de provar random
  
## Tipos de purevas  
* untiarias
  
Testean cada unitat independiente  
  
* integració
  
Testeja les unitats combinades
  
* sistema  
  
Testeja tot el sistema  
  
* d'aceptació   
  
Testejació per part del client  
  
* Alpha  
  
--  
  
* Beta
  
--  
  
* Regressió
  
--  
  
  
## Tipos de pruevas  
  
* Estaticas
  
Sistema en no ejecución.  
  
* Dinamicas
  
Sistema en ejecución  
  
  
### Segun a lo que podemos aceder  
  
* de caja negra
  
No tenemos aceso al codigo que se ejecuta.  
  
* de caja blanca
  
Podemos ver como esta diseñada e implementada la aplicación.  
  
  
### Pruevas unitarias  
  
* Codigo luego pruevas
* TDD (Test Driven Development) pruevas luego codigo  
  
  
### De regresión/ humo  
  
* Regresión  
Pruevas basicas de comprovación que se pasan regularmente (no son demasiado exaustivas)  
* de humo
Pruevas mas intensivas para enseñarselo a un cliente o alguien importante.  
  
  
## Beneficios de testing  
  
Testear diferentes inputs en funciones.  
  
Si las pruevas no son de calidad, puedes hacer muchas pruevas y no encontrar errores.  
  
  
### De caja negra  
  
Provaremos los casos mas diferentes.  
  
  
### Clases de equivalencia  
  
De la función año bisiesto, provaremos:  
* multiples de 4
* 5
* 100
* ...
  
Meses:  
* -02,-01,00
* 01-12
* 13,14...
  
  
### Valores limite  
  
Es un refinamiento de las clases de equivalencia.  
Suele ser donde hay problemas.  
* <=
* = 
* >=
  
  
### Datos aleatorias  
  
Si no tienes tiempo son una buena opción.  
  
  
## Ejemplo  
  
* >=5
* <= 15
	* Letras 
		* mayusculas
		* minusculas
	* numeros
	* gion
* el guion no puede estar al principio ni al final, pero peude haver varios consecutivos
* ha de contenir al menos una letra
* no puede ser 1(o varias) de las parablas reservadas
  
#### Tests para ejemplo  
  

