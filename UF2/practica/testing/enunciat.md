Crear un joc de proves funcional (de capsa negra) per la funció CPdescrip(codpos integer),  que introduint un codi postal retorna el nom de la província i del municipi corresponent al codi.

Com a precondició s'estableix que l'entrada serà numèrica.

El programa accedirà a una estructura de dades que guarda el codi postal,  el nom del municipi i el nom de la provincia. El codi postal acabat en tres zeros correspon a la província. (Els dos primers dígits determinen província. Els 3 últims municipi)

Recordeu que a Espanya hi ha 52 províncies.

Per cada cas de prova, heu de determinar també la sortida esperada.  

